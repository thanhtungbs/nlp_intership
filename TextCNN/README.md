# TextCNN
# Implementing a CNN for Text Classification in TensorFlow

## Project Structure

 .
    ├── config                  # Config files (.yml, .json) using with hb-config
    ├── data                    # dataset path
    ├── notebooks               # Prototyping with numpy or tf.interactivesession
    ├── scripts                 # download or prepare dataset using shell scripts
    ├── text-cnn                # text-cnn architecture graphs (from input to logits)
        ├── __init__.py             # Graph logic
    ├── data_loader.py          # raw_date -> precossed_data -> generate_batch (using Dataset)
    ├── hook.py                 # training or test hook feature (eg. print_variables)
    ├── main.py                 # define experiment_fn
    ├── model.py                # define EstimatorSpec
    └── predict.py              # test trained model 

