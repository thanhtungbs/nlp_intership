## There are two main types of models available in Keras: 

- The Sequential model.
- The Model class used with the functional API.

### These models have a number of method and attributes in common:

- _model.layers_ is a flattened list of layers comprising the model
- _model.inputs_ is the list of input tensors of the model
- _model.outputs_ is the list of output tensors of the model
- _model.summary()_ prints a summary representation of your model. Shortcut for utils.print_summary
- _model.get_config()_ returns a dictionary containing the configuration of the model. The model can be reinstantiated from its config via:

```python
config = model.get_config()
model = Model.from_config(config)
# or, for Sequential:
model = Sequential.from_config(config)
```	
- `model.get_weights()` returns a list of all weight tensors in the model, as Numpy arrays.
- `model.set_weights(weights)` sets the values of the weights of the model, from a list of Numpy arrays. The arrays in the list should have the same shape as those returned by get_weights().
- `model.to_json()` returns a representation of the model as a JSON string. Note that the representation does not include the weights, only the architecture. You can reinstantiate the same model (with reinitialized weights) from the JSON string via:

```python
from keras.models import model_from_json

json_string = model.to_json()
model = model_from_json(json_string)
```